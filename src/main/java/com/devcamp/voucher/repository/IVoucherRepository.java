package com.devcamp.voucher.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.voucher.models.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
