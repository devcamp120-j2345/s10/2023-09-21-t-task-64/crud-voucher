package com.devcamp.voucher.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucher.models.CVoucher;
import com.devcamp.voucher.repository.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {

    @Autowired
	IVoucherRepository pVoucherRepository;

    @GetMapping("/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVouchers() {
		try {
			List<CVoucher> pVouchers = new ArrayList<CVoucher>();
			
            pVoucherRepository.findAll().forEach(pVouchers::add);

			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
        try{
            Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
            if (voucherData.isPresent()) {
                return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


    @PostMapping("/vouchers")
	public ResponseEntity<CVoucher> createCVoucher(@Valid @RequestBody CVoucher pVouchers) {
		try {
            CVoucher voucher = new CVoucher();
            voucher.setMaVoucher(pVouchers.getMaVoucher());
            voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
            voucher.setGhiChu(pVouchers.getGhiChu());
            voucher.setNgayTao(new Date());

            voucher = pVoucherRepository.save(voucher);

			return new ResponseEntity<>(voucher, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> updateCVoucherById(@Valid @PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
		
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
            CVoucher uVoucher = voucherData.get();
            uVoucher.setMaVoucher(pVouchers.getMaVoucher());
            uVoucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
            uVoucher.setGhiChu(pVouchers.getGhiChu());
            uVoucher.setNgayCapNhat(new Date());
            
            uVoucher = pVoucherRepository.save(uVoucher);

			return new ResponseEntity<>(uVoucher, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @DeleteMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @DeleteMapping("/vouchers")
	public ResponseEntity<CVoucher> deleteAllCVoucher() {
		try {
			pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
